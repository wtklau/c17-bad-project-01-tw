import express from 'express';
import { knex } from './app';
export const homePageRoutes = express.Router()


// GET
homePageRoutes.get('/', getMovies)
// homePageRoutes.get('/:id', getMovie);

async function getMovies(req: express.Request, res: express.Response) {
    const limit = 12;
    const offset = Number(req.query.index) * limit
    const queryResult = await knex
    .select('description', 'title', 'rank', 'imageURL', 'id', 'also_view', 'asin')
    .from('movies')
    .orderBy('also_view', 'asc')
    .offset(offset)
    .limit(limit);

    const movies = queryResult;
    res.json({ data: movies});
}
