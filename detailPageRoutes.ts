import express from 'express';
import { knex } from './app';
// import { tables } from './app';
export const detailPageRoutes = express.Router()


// GET
detailPageRoutes.get('/:id', getDetail)

async function getDetail(req: express.Request, res: express.Response) {
  console.log(req.params.id)
  const movieId = req.params.id

  const queryResult = await knex
    .select('description', 'title', 'rank', 'imageURL', 'reviewerName', 'reviewText', 'vote', 'reviewerID')
    .from('movies')
    .leftJoin('reviews', 'reviews.asin', 'movies.asin')
    .where('movies.id', '=', movieId);

  const details = queryResult;
  res.json({ data: details });
}



//post
detailPageRoutes.post("/:id", async function (req, res) {
  console.log("detailPageRoutes")
  let inputText = req.body.reviewText;
  let movieId = req.params.id
  let reviewerName = req.session['user'].username
  let reviewerId = req.session['user'].reviewerId
  console.log("reviewText--->", inputText);
  console.log("userId--->", reviewerName)
  console.log("movie_id--->", movieId)

  if (!movieId || !reviewerName) {
    return res.status(400).json({ message: "missing movieId or reviewerName" });
  } else {

    try {
      var asin = await knex
        .select('asin')
        .from('movies')
        .where('id', '=', movieId);
      if (asin.length != 1) {
        return res.status(400).json({ message: "internal error" })
      }
      asin = asin[0].asin
      console.log('!!!!!', asin)
      const insertReviewText = await knex
        .insert({
          reviewText: inputText,
          reviewerName: reviewerName,
          reviewerID: reviewerId,
          asin: asin
        })
        .into('reviews')

      console.log('***write review', insertReviewText)
      return res.json({ message: "success" });
    } catch (err) {
      return res.status(400).json({ message: "internal error" })
    }
  }
})







